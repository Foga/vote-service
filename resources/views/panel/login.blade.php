@extends('layouts.panel')

@section('title')
    Панель администратора
@endsection

@section('extend-css')
    <link href="{{ URL::asset('css/signin.css') }}" rel="stylesheet">
@endsection

@section('main-content')
    <div class="container">

        {!! Form::open(['url' => '/admin/login', 'class' => 'form-signin']) !!}
            <h2 class="form-signin-heading">Пожалуйста, авторизуйтесь</h2>
            {!! Form::text('login', '', array('class' => 'form-control', 'placeholder' => 'Логин')) !!}
            {!! Form::password('password', array('class' => 'form-control', 'placeholder' => 'Пароль')) !!}

            <div class="checkbox">
                <label>
                    {!! Form::checkbox('remember-me') !!} Запомнить меня
                </label>
            </div>

            {!! Form::submit('Авторизация', array('class' => 'btn btn-lg btn-primary btn-block')) !!}
        {!! Form::close() !!}

    </div> <!-- /container -->
@endsection
