@extends('layouts.panel')

@section('title')
    Панель администратора
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
                @include('panel.sidebar')
            </nav>

            <main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
                <h1>Изменить голосование - {{ $vote->name }}</h1>

                @if ($errors->any())

                    <div class="alert alert-danger" role="alert">
                        @foreach($errors->all(':message') as $error)
                            {!! $error !!} <br>
                        @endforeach
                    </div>

                @endif

                {!! Form::open(['url' => '/admin/votes/'.$vote->id, 'class' => 'form-signin']) !!}

                <div class="form-group">
                    {!! Form::label('Вопрос голосования') !!}
                    {!! Form::text('name', $vote->name, array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Тип голосования') !!}
                    <select class="form-control" name="type">
                        <option value="radio" @if($vote->type == "radio") selected @endif>Один ответ</option>
                        <option value="checkbox" @if($vote->type == "checkbox") selected @endif>Несколько ответов</option>
                    </select>
                </div>

                <div class="form-group">
                    {!! Form::label('Варианты ответов') !!}

                    <ul class="list-group answers-list">

                    </ul>

                    <a href="#" class="btn btn-secondary mt-3 add-answer">Добавить ответ</a>

                    <input type="hidden" class="form-control" name="fields">
                </div>

                {!! Form::submit('Сохранить голосование', array('class' => 'btn btn-lg btn-primary btn-block')) !!}

                {!! Form::close() !!}
            </main>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            var fields = {!! $vote->fields !!};

            $('.add-answer').on('click', function() {
                var item = '<li class="list-group-item form-inline" data-id="'+"new"+fields.length+'"><div class="input-group"><input type="text" class="form-control answer" placeholder="Текст ответа" ><span class="input-group-btn"><a href="#" class="btn btn-danger delete-answer">Удалить</a></span></div></li>';

                fields.push({"id": "new"+fields.length, "value": ""});

                $('.answers-list').append(item);

                $('.delete-answer').on('click', function() {
                    var answer_id = $(this).parent().parent().parent().attr('data-id');

                    $('.list-group-item').each(function() {
                        if($(this).attr('data-id') == answer_id) {
                            $(this).remove();

                            fields = fields.filter(function(field) {
                                return field.id != answer_id;
                            });

                            $('input[name=fields]').val(JSON.stringify(fields));
                        }
                    });
                });

                updateJSON();
            });

            function updateAnswers() {
                for(var i = 0; i < fields.length; i++) {
                    var item = '<li class="list-group-item form-inline" data-id="'+fields[i].id+'"><div class="input-group"><input type="text" class="form-control answer" placeholder="Текст ответа" value="'+fields[i].value+'"><span class="input-group-btn"><a href="#" class="btn btn-danger delete-answer">Удалить</a></span></div></li>';

                    $('.answers-list').append(item);

                    $('input[name=fields]').val(JSON.stringify(fields));
                }
            }

            console.log(fields);

            updateAnswers();
            updateJSON();

            function updateJSON() {
                $('input.answer').on('keyup', function() {
                    var answer_id = $(this).parent().parent().attr('data-id');

                    for(var i = 0; i < fields.length; i++) {
                        if(fields[i].id == answer_id) {
                            fields[i].value = $(this).val();

                            console.log(fields);

                            $('input[name=fields]').val(JSON.stringify(fields));
                        }
                    }
                });
            }

            $('.delete-answer').on('click', function() {
                var answer_id = $(this).parent().parent().parent().attr('data-id');

                $('.list-group-item').each(function() {
                    if($(this).attr('data-id') == answer_id) {
                        $(this).remove();

                        fields = fields.filter(function(field) {
                            return field.id != answer_id;
                        });

                        $('input[name=fields]').val(JSON.stringify(fields));
                    }
                });
            });
        });
    </script>
@endsection