@extends('layouts.panel')

@section('title')
    Панель администратора
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
                @include('panel.sidebar')
            </nav>

            <main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
                <h1>Голосования</h1>

                <a  class="btn btn-primary" href="/admin/votes/create">Добавить голосование</a>

                <table class="table table-striped table-inverse mt-3 table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Название</th>
                        <th>Тип</th>
                        <th>Опции</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($votes as $vote)
                        <tr>
                            <th scope="row">{{ $vote->id }}</th>
                            <td>{{ $vote->name }} @if($vote->active) <span class="badge badge-primary">Активно</span> @endif</td>
                            <td>{{ $vote->type }}</td>
                            <td width="250">
                                <div class="btn-group btn-group-sm" role="group">

                                    @if(!$vote->active)
                                        <a href="/admin/votes/{{ $vote->id }}/active" class="btn btn-primary">Сделать активным</a>
                                    @endif

                                    <a href="/admin/votes/{{ $vote->id }}" class="btn btn-secondary">Редактировать</a>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </main>
        </div>
    </div>
@endsection