@extends('layouts.panel')

@section('title')
    Панель администратора
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
                @include('panel.sidebar')
            </nav>

            <main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
                <h1>Панель администратора</h1>

            </main>
        </div>
    </div>
@endsection