@extends('layouts.app')

@section('title')
    Система голосований
@endsection

@section('main-content')
    <div class="container" style="margin-top: 80px">
        <div class="row">
            <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">{{ $vote->name }}</h4>
                <ul class="vote-container" style="list-style: none; padding: 15px;">
                    {{ csrf_field() }}

                    @foreach($vote->fields as $field)
                        @if($vote->type == "checkbox")
                            <li class="vote-item" data-id="{{ $field->id }}">
                                <div class="vote-result" style="position: relative; display: none;">
                                    <div class="vote-percent">38%</div>
                                    <div class="vote-line" style="width: 100%; height: 2px; background-color: black"></div>
                                </div>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" name="vote" value="{{ $field->id }}" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">{{ $field->name }}</span>
                                </label>
                            </li>
                        @endif
                        @if($vote->type == "radio")
                            <li class="vote-item" data-id="{{ $field->id }}">
                                <div class="vote-result" style="position: relative; display: none;">
                                    <div class="vote-percent">38%</div>
                                    <div class="vote-line" style="width: 100%; height: 2px; background-color: black"></div>
                                </div>
                                <label class="custom-control custom-radio">
                                    <input id="radioStacked3" name="vote" type="radio" value="{{ $field->id }}" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">{{ $field->name }}</span>
                                </label>
                            </li>
                        @endif
                    @endforeach
                </ul>
                <hr>
                <p class="mb-0">
                <span class="thanks-vote" style="display: none;">Спасибо, ваш голос учтен!</span>
                <div class="btn-group btn-group-sm vote-btns" role="group">
                    <a href="#" class="btn btn-primary vote-action">Голосовать</a>
                    <a href="#" class="btn btn-info vote-results">Результаты</a>
                </div>
                </p>
            </div>
        </div>
    </div>

    <script>
        $(function() {

            var user_voted = false;

            @if($vote->type == "checkbox")
                $('.vote-action').on('click', function() {
                    var selectedAnswers = [];

                    $("input:checkbox[name ='vote']:checked").each(function() {
                       selectedAnswers.push($(this).val());
                    });

                    var token = $('meta[name="csrf-token"]').attr('content');

                    if(selectedAnswers.length > 0) {
                        $.ajax({
                            type:'POST',
                            url: './vote/{{ $vote->id }}',
                            dataType: 'JSON',
                            data: {
                                "_method": 'POST',
                                "_token": token,
                                'answer_id': selectedAnswers
                            },
                            success:function(data){
                                user_voted = true;

                                if(data.success) {
                                    showResults();
                                }
                            },
                        });
                    }
                });

                $('.vote-results').on('click', function() {
                    showResults();
                });
            @endif

            @if($vote->type == "radio")
                $('.vote-action').on('click', function() {
                    var selectedAnswer = $("input:radio[name ='vote']:checked").val();
                    var token = $('meta[name="csrf-token"]').attr('content');

                    if(selectedAnswer) {
                        $.ajax({
                            type:'POST',
                            url: './vote/{{ $vote->id }}',
                            dataType: 'JSON',
                            data: {
                                "_method": 'POST',
                                "_token": token,
                                'answer_id': selectedAnswer
                            },
                            success:function(data){
                                user_voted = true;

                                if(data.success) {
                                    showResults();
                                }
                            },
                        });
                    }
                });

                $('.vote-results').on('click', function() {
                   showResults();
                });
            @endif

            function showResults() {
                $(".custom-control input, .custom-control .custom-control-indicator, .vote-btns").remove();

                if(user_voted) {
                    $('.thanks-vote').show();
                }

                var token = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    type:'POST',
                    url: './vote/{{ $vote->id }}/results',
                    dataType: 'JSON',
                    data: {
                        "_method": 'POST',
                        "_token": token
                    },
                    success:function(data){

                        for(var i = 0; i < data.length; i++) {
                            $('.vote-item').each(function() {
                                console.log($(this).attr('data-id'));

                                if($(this).attr('data-id') == data[i].id) {
                                    $(this).find('.vote-percent').html(data[i].percent+"% ("+data[i].count+")");
                                    $(this).find('.vote-line').css('width', data[i].percent+"%");
                                    $('.vote-result').show();
                                }
                            });
                        }

                    },
                });
            }

        });
    </script>

@endsection