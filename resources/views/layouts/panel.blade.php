<html lang="{{ app()->getLocale() }}"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Vote Service  - @yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/dashboard.css') }}" rel="stylesheet">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link href="{{ URL::asset('libs/tags-input/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ URL::asset('libs/tags-input/bootstrap-tagsinput.js') }}"></script>

    @yield('extend-css')
</head>

<body>

@include('other.navbar')

@yield('main-content')

</body>
</html>