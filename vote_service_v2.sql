/*
Navicat MySQL Data Transfer

Source Server         : localhostMySQL
Source Server Version : 50716
Source Host           : localhost:3306
Source Database       : vote_service

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2017-09-18 23:01:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', '$2y$10$/BdwU7dwvswsUuxdar6uYOq0gTxLPkLl9pJ7MNYaQC0IUBEjGmmd.', '7rrTsqHkMeuG6i3726SEFgVkDaw9vPVVEL6Zmseq9Tw35EvMY1YSD3JuPTHr', '2017-09-18 18:23:50', '2017-09-18 18:23:50');

-- ----------------------------
-- Table structure for votes
-- ----------------------------
DROP TABLE IF EXISTS `votes`;
CREATE TABLE `votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of votes
-- ----------------------------
INSERT INTO `votes` VALUES ('1', 'Сколько денег в месяц вам нужно для счастья?', 'radio', '1', '2017-09-18 22:58:28', '2017-09-18 19:58:28');
INSERT INTO `votes` VALUES ('3', 'Выросла ли ваша зарплата за последний год?', 'radio', '0', '2017-09-18 22:58:28', '2017-09-18 19:58:28');
INSERT INTO `votes` VALUES ('7', 'Вы когда-нибудь сталкивались с овербукингом?', 'checkbox', '0', '2017-09-18 22:57:38', '2017-09-18 19:57:38');

-- ----------------------------
-- Table structure for vote_fields
-- ----------------------------
DROP TABLE IF EXISTS `vote_fields`;
CREATE TABLE `vote_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vote_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vote_fields
-- ----------------------------
INSERT INTO `vote_fields` VALUES ('1', '3', 'Да, наша компания регулярно повышает оплату труда сотрудников', '0', '2017-09-18 22:57:36', '2017-09-18 19:57:36');
INSERT INTO `vote_fields` VALUES ('2', '3', 'Сама зарплата не увеличилась, но часто получаю премии и бонусы', '0', '2017-09-18 22:57:36', '2017-09-18 19:57:36');
INSERT INTO `vote_fields` VALUES ('3', '3', 'Нет, она осталась прежней', '1', '2017-09-18 22:57:41', '2017-09-18 19:57:41');
INSERT INTO `vote_fields` VALUES ('15', '7', 'Да, узнавал(а) на регистрации, что мест на самолет больше нет', '5', '2017-09-18 22:54:54', '2017-09-18 19:54:54');
INSERT INTO `vote_fields` VALUES ('16', '7', 'Я не сталкивался(лась), но знакомые не смогли улететь из-за нехватки мест', '9', '2017-09-18 22:55:22', '2017-09-18 19:55:22');
INSERT INTO `vote_fields` VALUES ('17', '7', 'Нет, все полеты проходили нормально', '5', '2017-09-18 22:54:54', '2017-09-18 19:54:54');
INSERT INTO `vote_fields` VALUES ('18', '3', 'Наоборот, снизилась', '0', '2017-09-18 19:57:36', '2017-09-18 19:57:36');
INSERT INTO `vote_fields` VALUES ('19', '1', 'Ста тысяч рублей вполне достаточно', '0', '2017-09-18 19:58:26', '2017-09-18 19:58:26');
INSERT INTO `vote_fields` VALUES ('20', '1', 'Миллиона хватит', '0', '2017-09-18 19:58:26', '2017-09-18 19:58:26');
INSERT INTO `vote_fields` VALUES ('21', '1', 'Я не требовательный, мне зарплаты хватает', '0', '2017-09-18 19:58:26', '2017-09-18 19:58:26');
INSERT INTO `vote_fields` VALUES ('22', '1', 'Сколько ни давай, все равно не хватит', '0', '2017-09-18 19:58:26', '2017-09-18 19:58:26');
SET FOREIGN_KEY_CHECKS=1;
