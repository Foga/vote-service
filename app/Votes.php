<?php
/**
 * Created by PhpStorm.
 * User: Foga2H
 * Date: 18.09.2017
 * Time: 16:35
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Votes extends Model
{
    protected $table = "votes";

    public static function createVote(Request $request) {

        $vote = new Votes();
        $vote->name = $request->input('name');
        $vote->type = $request->input('type');
        $vote->save();

        return $vote;
    }

    public static function updateVote(Request $request, $id = null) {

        $vote = static::find($id);
        $vote->name = $request->input('name');
        $vote->type = $request->input('type');
        $vote->save();

        return $vote;
    }

    public static function getActiveVote() {
        $vote = static::where('active', true)->first();
        $vote->fields = VoteFields::where('vote_id', $vote->id)->get();

        return $vote;
    }

    public static function setAllInactive() {
        static::where('active', true)->update(array('active' => false));
    }
}