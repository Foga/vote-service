<?php
/**
 * Created by PhpStorm.
 * User: Foga2H
 * Date: 18.09.2017
 * Time: 15:14
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\LoginController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Validator;

class PanelController extends Controller
{
    public static function getIndex() {
        if(Auth::guest())
            return View::make('panel.login');

        return redirect('/admin/votes');
    }

    public static function getLogout() {
        if(!Auth::guest())
            Auth::logout();

        return redirect('/admin/');
    }

    public static function postLogin(Request $request) {

        $validator = Validator::make($request->all(), [
            'login' => 'required|max:255',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('admin/')
                ->withErrors($validator)
                ->withInput();
        }

        if (Auth::attempt(['login' => $request->input('login'), 'password' => $request->input('password')], $request->input('remember-me'))) {
            return redirect('/admin/');
        }

        return redirect('/admin/')->withInput(array('error' => 'Неправильный логин/пароль'));
    }
}