<?php
/**
 * Created by PhpStorm.
 * User: Foga2H
 * Date: 18.09.2017
 * Time: 18:15
 */

namespace App\Http\Controllers;


use App\VoteFields;
use App\Votes;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Validator;

class VotesController extends Controller
{
    public static function getIndex() {

        $votes = Votes::all();

        return View::make('panel.votes.index')->with(array('votes' => $votes));
    }

    public static function getCreate() {
        return View::make('panel.votes.create');
    }

    public static function getSetActive($id = null) {
        $vote = Votes::find($id);

        if($vote) {
            Votes::setAllInactive();

            $vote->active = true;
            $vote->save();
        }

        return redirect('admin/votes');
    }

    public static function getEdit($id = null) {
        $vote = Votes::find($id);

        if(!$vote)
            return redirect('admin/votes');

        $fields = [];

        foreach(VoteFields::where('vote_id', $vote->id)->get() as $field) {
            $fields[] = (object) [
                'id' => $field->id,
                'value' => $field->name
            ];
        }

        $vote->fields = json_encode($fields, JSON_HEX_QUOT);

        return View::make('panel.votes.edit')->with(array('vote' => $vote));
    }

    public static function postSave($id = null, Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'type' => 'required',
            'fields' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('admin/votes/create')
                ->withErrors($validator)
                ->withInput();
        }

        $vote = Votes::updateVote($request, $id);
        $fields = VoteFields::updateFields($vote, $request->input('fields'));

        if($fields && $vote) {
            return redirect('admin/votes')->with(array('message' => 'Голосование успешно сохранено.'));
        }

        return redirect('admin/votes')->with(array('message' => 'Произошла ошибка, что-то пошло не так.'));
    }

    public static function postCreate(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'type' => 'required',
            'fields' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('admin/votes/create')
                ->withErrors($validator)
                ->withInput();
        }

        $vote = Votes::createVote($request);
        $fields = VoteFields::createFields($vote, $request->input('fields'));

        if($fields && $vote) {
            return redirect('admin/votes')->with(array('message' => 'Новое голосование успешно создано.'));
        }

        return redirect('admin/votes')->with(array('message' => 'Произошла ошибка, что-то пошло не так.'));
    }

    public static function postVote($id = null, Request $request) {
        VoteFields::vote($id, $request->input('answer_id'));

        return response()->json([
            'success' => true,
        ]);
    }

    public static function postResults($id = null) {
        $results = VoteFields::results($id);

        return response()->json($results);
    }
}