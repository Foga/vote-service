<?php
/**
 * Created by PhpStorm.
 * User: Foga2H
 * Date: 18.09.2017
 * Time: 21:34
 */

namespace App\Http\Controllers;


use App\Votes;
use Illuminate\Support\Facades\View;

class IndexController extends Controller
{
    public static function getIndex() {

        $active_vote = Votes::getActiveVote();

        return View::make('pages.index')->with(array('vote' => $active_vote));
    }
}