<?php
/**
 * Created by PhpStorm.
 * User: Foga2H
 * Date: 18.09.2017
 * Time: 18:47
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Response;
use App\Votes;

class VoteFields extends Model
{
    protected $table = "vote_fields";

    public static function createFields(Votes $vote, $fields) {

        $fields = json_decode($fields);

        foreach($fields as $field) {

            $v_field = new VoteFields();
            $v_field->vote_id = $vote->id;
            $v_field->name = $field->value;
            $v_field->save();

        }

    }

    public static function vote($id, $answer_id) {
        $vote = Votes::find($id);

        if(is_array($answer_id)) {
            if($vote) {
                foreach($answer_id as $answer) {
                    $vote_field = static::where('vote_id', $id)->where('id', $answer)->first();
                    $vote_field->count = $vote_field->count+1;
                    $vote_field->save();
                }
            }
        } else {
            if($vote) {
                $vote_field = static::where('vote_id', $id)->where('id', $answer_id)->first();
                $vote_field->count = $vote_field->count+1;
                $vote_field->save();
            }
        }

        return true;
    }

    public static function results($id) {
        $vote = Votes::find($id);

        if($vote) {
            $results = [];

            $vote_fields = static::where('vote_id', $vote->id)->get();

            $full_count = 0;

            foreach($vote_fields as $v_field) {
                $full_count = $full_count + $v_field->count;
            }

            foreach($vote_fields as $v_field) {
                $results[] = (object) [
                    'id' => $v_field->id,
                    'percent' => ($v_field->count != 0) ? round(100/($full_count/$v_field->count), 2) : 0,
                    'count' => $v_field->count
                ];
            }

            return $results;
        }

        return false;
    }

    public static function updateFields(Votes $vote, $fields) {

        $fields = json_decode($fields);

        $ids = [];

        foreach($fields as $field) {

            $has_field = static::where('id', $field->id)->where('vote_id', $vote->id)->first();

            if(!$has_field) {
                $has_field = new VoteFields();
                $has_field->vote_id = $vote->id;
                $has_field->name = $field->value;
                $has_field->save();

                $ids[] = $has_field->id;
            } else {
                $has_field = static::find($field->id);
                $has_field->name = $field->value;
                $has_field->save();

                $ids[] = $has_field->id;
            }
        }

        if(!empty($ids))
            static::whereRaw('vote_fields.id NOT IN ('.implode(',', $ids).')')->where('vote_id', $vote->id)->delete();

        return true;
    }
}