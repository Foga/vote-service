<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@getIndex');

Route::post('/vote/{id}', 'VotesController@postVote');
Route::post('/vote/{id}/results', 'VotesController@postResults');

Route::prefix('admin')->group(function () {
    Route::get('/', ['as' => 'login', 'uses' => 'PanelController@getIndex']);

    Route::post('/login', 'PanelController@postLogin');

    Route::group(['middleware' => 'auth'], function () {

        Route::get('/votes', 'VotesController@getIndex');
        Route::get('/votes/create', 'VotesController@getCreate');

        Route::get('/votes/{id}/active', 'VotesController@getSetActive');
        Route::get('/votes/{id}', 'VotesController@getEdit');

        Route::post('/votes/create', 'VotesController@postCreate');
        Route::post('/votes/{id}', 'VotesController@postSave');
        Route::get('/logout', 'PanelController@getLogout');
    });
});